LoveToAndroid is a collection of tools to make it easy to port your L�VE game to Android.

Steps:
1. Create zip with your game, rename to "game.love" in this folder
2. Start 2_love2apk.bat - this will create "game.apk"
3. Copy "game.apk" to your Android device and open it in the file browser to install

That is it.




OPTIONAL Steps:
4. Replace icon.png (144x144px png) and run 2_love2apk.bat again.
5. Edit tools\love-android-sdl2\AndroidManifest.xml to change name and other details
6. For your apk to have an own ID, not only edit AndroidManifest.xml but also adjust the folder/file names and file content of
tools\love-android-sdl2\src\love\
tools\love-android-sdl2\src\love\to\
tools\love-android-sdl2\src\love\to\android
tools\love-android-sdl2\src\love\to\android\LtaActivity.java
7. Oh yeah, you might want to create a backup of
tools\love-android-sdl2\
so you can use it for other projects

For more, for now, check love-android-sdl2 wiki for more info or ask in the love2d.org LoveToAndroid forum thread.

Background info:
tools\ant is from https://ant.apache.org/bindownload.cgi ; removed documentation, maybe more
tools\jdk is from http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html JDK, NOT JRE, NOT JAVA PLATFORM , openjdk didn't work; removed as much as I could without breaking ant debug
tools\android is downloaded with Android SDK Manager https://developer.android.com/sdk/index.html SDK TOOLS ONLY, NOT ANDROID STUDIO; removed as much as possible without breaking ant debug
tools\love-android-sdl2 from https://bitbucket.org/MartinFelis/love-android-sdl2 , self-compiled early 2015

CREDITS
Collected by Iwan Gabovitch http://qubodup.net . Check subdirectories for licenses and credits