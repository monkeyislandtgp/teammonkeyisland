REM set variables
setlocal
set ANT_HOME=%~dp0\tools\ant
set JAVA_HOME=%~dp0\tools\jdk
set ANDROID_HOME=%~dp0\tools\android
set PATH=%PATH%;%ANT_HOME%\bin

REM cd and remove previoius output
del game.apk /q
cd tools\love-android-sdl2
rd gen /s/q
rd bin /s/q

REM easier game copying
IF EXIST ..\..\game.love (
del assets\game.love
copy ..\..\game.love assets\game.love
)

REM easier icon copying
IF EXIST ..\..\icon.png (
del res\drawable-xxhdpi\ic_launcher.png
copy ..\..\icon.png res\drawable-xxhdpi\ic_launcher.png
)

REM compile and move
call ant debug
copy bin\love_android_sdl2-debug.apk ..\..\game.apk
echo
echo Press any key to close
pause >nul