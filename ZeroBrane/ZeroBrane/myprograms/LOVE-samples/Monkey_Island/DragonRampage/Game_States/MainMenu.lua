MainMenu ={}
MainMenu.__index = MainMenu

function MainMenu.Create()
  local self ={}
  setmetatable(self,MainMenu)
  self.Canvas = Canvas.Create()
  self.Objects = {} 
  return self
end


function MainMenu:Load()
  -- Play Button
  PlayNormal = love.graphics.newImage("Graphics/Buttons/PlayNormal.png")
  PlayClicked = love.graphics.newImage("Graphics/Buttons/PlayPressed.png")
  PlayButton = RolloverButton.Create(1280/2 ,350 ,PlayNormal,PlayClicked)
  PlayButton:SetLevelIndex(2)
  self.Canvas:AddElement(PlayButton)
  
  -- Leadorboard Button
  LeadorboardNormal = love.graphics.newImage("Graphics/Buttons/Leadorboard.png")
  LeadorboardClicked = love.graphics.newImage("Graphics/Buttons/LeadorboardPressed.png")
  LeadorboardButton = RolloverButton.Create(1280/2,475,LeadorboardNormal,LeadorboardClicked)
  LeadorboardButton:SetLevelIndex(1)
  self.Canvas:AddElement(LeadorboardButton)
  
  -- Help Button
  HelpNormal = love.graphics.newImage("Graphics/Buttons/Help.png")
  HelpClicked = love.graphics.newImage("Graphics/Buttons/HelpPressed.png")
  HelpButton = RolloverButton.Create(1280/2,600 ,HelpNormal,HelpClicked)
  HelpButton:SetLevelIndex(1)
  self.Canvas:AddElement(HelpButton)

  --Createing Backgound
  self.Objects.Background_Texture = love.graphics.newImage("Graphics/Backgrounds/Background.png")
  self.Objects.Foreground_Border = love.graphics.newImage("Graphics/Backgrounds/Border.png")
  self.Objects.Foreground_Corners= love.graphics.newImage("Graphics/Backgrounds/BorderCorners.png")
  self.Objects.Title= love.graphics.newImage("Graphics/Titles/Logo.png")
end

function MainMenu:Unload()
  self.Canvas:Unload()
end

function MainMenu:Update(dt)
  self.Canvas:Update(dt)
end

function MainMenu:Draw()
  love.graphics.draw(self.Objects.Background_Texture,0,0)
  love.graphics.draw(self.Objects.Foreground_Border,0,0)
  love.graphics.draw(self.Objects.Foreground_Corners,0,0)
  love.graphics.draw(self.Objects.Title,1280/2 - (706/2)  ,50)
  
  self.Canvas:Draw()
end

function love.touchpressed(id, x, y, dx, dy, pressure)
  HelpButton:InputLocation(x,y)
  PlayButton:InputLocation(x,y)
  LeadorboardButton:InputLocation(x,y)
end

