LevelOne ={}
LevelOne.__index = LevelOne
currentFrame = 1
elapsed = 0

function LevelOne.Create()
  local self ={}
  setmetatable(self,LevelOne)
  self.Canvas = Canvas.Create()
  self.Objects = {} 
  return self
end


function LevelOne:Load()
  width = 1280
  height = 720
  --Need This for mobile
  if love.system.getOS() == "Android" then
    local x, y = love.graphics.getDimensions()
    scaleX = x / width
    scaleY = y / height
  else
    scaleX = 1
    scaleY = 1
  end
  
  DargonFireBurst = {}
  DargonFireBurst.DargonFireBurst1 = love.graphics.newImage("Graphics/Sprites/Player/Dragonfire.png")
  
  DragonFirePosY = 0 * scaleY 
  DragonFirePosX = 0 * scaleX
  FireCheck = false
  
  MouseStartY = 360
  MouseEndY = 0

 
  DragonPosY = MouseStartY * scaleY
  DragonPosX = 10
  
  love.mouse.setVisible(true)
  
  --Load Sprite Sheet
  DragonSprite = love.graphics.newImage("Graphics/Sprites/Player//Dragon_SSheet.png")
  DragonFrames = {
    love.graphics.newQuad(0, 0, 96, 107, DragonSprite:getWidth(), DragonSprite:getHeight()),
    love.graphics.newQuad(96, 0, 96, 107, DragonSprite:getWidth(), DragonSprite:getHeight()),
    love.graphics.newQuad(192, 0, 96, 107, DragonSprite:getWidth(), DragonSprite:getHeight()),
    love.graphics.newQuad(288, 0, 96, 107, DragonSprite:getWidth(), DragonSprite:getHeight()),
  }
  
  --Need this for mobile
  love.window.setMode(width * scaleX, height *scaleY)
end

function LevelOne:Unload()
  self.Canvas:Unload()
end

function LevelOne:Update(dt)
  DragonFirePosX = (DragonFirePosX + 15) * scaleX
  
  if (FireCheck == false)then
    love.graphics.draw(DargonFireBurst.DargonFireBurst1, DragonFirePosX, DragonFirePosY , 0 ,(1*scaleX), (1*scaleY))
  elseif (FireCheck == true and DragonFirePosX< 170)then
    FireCheck = false
  end 
  
  
  --Sets the Animation Speed
  elapsed = elapsed + 0.1
  if elapsed > 1 then
      elapsed = elapsed - 1
      if currentFrame == 4 then
        currentFrame = 1
      else
        currentFrame = currentFrame + 1
      end
  end
  
  
  
  self.Canvas:Update(dt)
end

function LevelOne:Draw()
  
  self.Canvas:Draw()
  --draw
  love.graphics.setBackgroundColor(255, 255, 255, 255)

  love.graphics.draw(DargonFireBurst.DargonFireBurst1, DragonFirePosX, DragonFirePosY , 0 ,(1*scaleX), (1*scaleY))
  love.graphics.draw(DragonSprite, DragonFrames[currentFrame], DragonPosX,    DragonPosY, 0 ,(1*scaleX), (1*scaleY))

end

function love.mousepressed( x, y, dx, dy, istouch )
  MouseStartY = y
  
  
  if (FireCheck == false)then
    DragonFirePosX = DragonPosX + 40
    DragonFirePosY = DragonPosY 
  end
end

function love.mousereleased(x, y, button, istouch)
  
  MouseEndY = MouseStartY - y
  
  if MouseEndY < -20 then
    DragonPosY = 720-107
  elseif MouseEndY > 20 then
    DragonPosY = 0 
  else 
    DragonPosY = 360 - 107
  end
end

function love.touchmoved( id, x, y, dx, dy, pressure )
  
  DragonPosX = ((x* love.graphics.getWidth()) + 96/2)
  DragonPosY =  ((y* love.graphics.getWidth()) + 107/2)
  
end
function love.touchpressed(id, x, y, dx, dy, pressure)

  if (FireCheck == false)then
    

    DragonFirePosX = DragonPosX + 40
    DragonFirePosY = DragonPosY 

   
  end
end



