State ={}
State.__index = State

function State.Create(title, level)
  local self ={}
  setmetatable(self,State) 
  --Creates an instace of the variables to local
  self.Title = title
  self.Level = level
  return self
end

function State:Load()
  self.Level:Load()
end

function State:Unload()
  self.Level:Unload()
end

function State:Update()
  self.Level:Update()
end

function State:Draw()
  self.Level:Draw()
  love.graphics.print("State " .. tostring(self.Title))
end

