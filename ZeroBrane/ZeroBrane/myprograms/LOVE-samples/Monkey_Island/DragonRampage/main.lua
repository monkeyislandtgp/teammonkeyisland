-- GameState
require("State_Managing/GameState")
require("State_Managing/State")

--States
require("Game_States/MainMenu")
require("Game_States/PauseMenu")
require("Game_States/LevelOne")
require("Game_States/LevelTwo")
require("Game_States/GameOver")
require("Game_States/BoosterMenu")

--Interface
require("Interface/RolloverButton")
require("Interface/Canvas")


function love.load()
  width = 1280
  height = 720
  if love.system.getOS() == "Android" then
    local x, y = love.graphics.getDimensions()
    scaleX = x / width
    scaleY = y / height
  else
    scaleX = 1
    scaleY = 1
  end
  
  Game_States = GameState.Create(1)
  
  --Main Menu State
  Game_States:AddState(State.Create("Main Menu",MainMenu.Create()))
  -- Level One State
  Game_States:AddState(State.Create("Level One",LevelOne.Create()))
  -- Level Two State
  Game_States:AddState(State.Create("Level Two",LevelTwo.Create()))
  -- Game Over State
  Game_States:AddState(State.Create("Game Over",GameOver.Create()))
  -- Pause Menu State
  Game_States:AddState(State.Create("Pause Menu",PauseMenu.Create()))
  -- Booster Menu State
  Game_States:AddState(State.Create("Booster Menu",BoosterMenu.Create()))
  
  love.window.setMode(width * scaleX, height *scaleY)
end

function love.update(dt)
  Game_States:Update(dt)
end

function love.draw()
  love.graphics.scale(scaleX,scaleY)
  Game_States:Draw(dt)
end

function love.keypressed(key)
  if(key == "1") then
    Game_States:ChangeState(1)
  end
  if(key == "2") then
    Game_States:ChangeState(2)
  end
  if(key == "3") then
    Game_States:ChangeState(3)
  end
  if(key == "4") then
    Game_States:ChangeState(4)
  end
  if(key == "5") then
    Game_States:ChangeState(5)
  end
    if(key == "6") then
    Game_States:ChangeState(6)
  end
end
