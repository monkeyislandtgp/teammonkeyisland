
RolloverButton = {}
RolloverButton.__index = RolloverButton

function RolloverButton.Create(x, y, NormalGraphic, HoverGraphic)
  local self ={}
  setmetatable(self,RolloverButton)
  self.X = x
  self.Y = y
  self.Normal_Graphic = NormalGraphic
  self.Hover_Graphic = HoverGraphic
  self.Level = 0
  self.Inside = false
  self.Scale = 1
  self.cx = 0
  self.cy = 0
    
  self.bhp = false
  self.bcp = false
  
  width = 1280
  height = 720
  if love.system.getOS() == "Android" then
    local x, y = love.graphics.getDimensions()
    scaleX = x / width
    scaleY = y / height
  else
    scaleX = 1
    scaleY = 1
  end

  --self.buttonHovered = love.audio.newSource("assets/Sound/ButtonClick.wav")
  --self.buttonHovered:setLooping(false)
  --self.buttonClicked = love.audio.newSource("assets/Sound/ButtonHover.wav")
  
  return self
end

function RolloverButton:Load()

end

function RolloverButton:Unload()
  
end

function RolloverButton:Update(dt)
  self.cx, self.cy = love.mouse.getPosition()
  
  if(Contains(self.cx* scaleX, self.cy* scaleY, self.X , self.Y ,self.Normal_Graphic:getWidth(),self.Normal_Graphic:getHeight(),1))then
    self.Inside = true
    Game_States:ChangeState(self.Level)
  else
    self.Inside = false
  end
  
end

function RolloverButton:InputLocation(x,y)
  self.cx = x
  self.cy = y
end
  
function RolloverButton:SetLevelIndex(level)
  self.Level = level
end

function RolloverButton:IsClicked()
  return self.Inside
end

function RolloverButton:SetScale(value)
  self.Scale = value
end

function RolloverButton:Draw()
  if(not self.Inside) then
    love.graphics.draw(self.Normal_Graphic,self.X,self.Y,0,self.Scale,self.Scale,self.Normal_Graphic:getWidth()/2,self.Normal_Graphic:getHeight()/2)
  else
     love.graphics.draw(self.Hover_Graphic,self.X,self.Y,0,self.Scale,self.Scale,self.Hover_Graphic:getWidth()/2,self.Hover_Graphic:getHeight()/2)
  end
end


--Button Mouse Colission
function Contains(m_x,m_y,p_x , p_y , width, height,Scale)
  local OriginalX = p_x - (width/2) *Scale
  local OriginalY = p_y - (height/2)*Scale
  local MaxX = OriginalX + width * Scale
  local MaxY = OriginalY + height * Scale
  
  if(m_x > OriginalX  and m_y > OriginalY)then
    if(MaxX  > m_x and MaxY  > m_y) then
        return true
      end
  end
  return false
end
