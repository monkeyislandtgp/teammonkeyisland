Canvas = {}
Canvas.__index = Canvas

function Canvas.Create()
  local self ={}
  setmetatable(self,Canvas)
  self.Elements = {}
  return self
end

---------------- Loads All The Element Data ---------
function Canvas:Load()
     for index,value in ipairs(self.Elements) do
        value:Load()
    end
end
---------------- UnLoads All The Element Data ---------
function Canvas:Unload()
      for index,value in ipairs(self.Elements) do
        value:Unload()
    end
end
---------------- Updates All The Element Data ---------
function Canvas:Update(dt)
    for index,value in ipairs(self.Elements) do
        value:Update(dt)
    end
end
---------------- Adds Element to list ---------
function Canvas:AddElement(element)
  table.insert(self.Elements,element)
end
---------------- Draws All Elements ---------
function Canvas:Draw()
    for index,value in ipairs(self.Elements) do
        value:Draw()
    end
end
