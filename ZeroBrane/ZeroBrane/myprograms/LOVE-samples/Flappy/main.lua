
-- Change Grpahics make my own
flappy = {}
pipeX = {}
pipeY = {}
angle = 0
current_state = "main_Menu"
currentScore = 0
highScore = 0

function love.load()
  
  --Load Graphics 
  
  font = love.graphics.newImageFont("sprites/Font.png",
    " abcdefghijklmnopqrstuvwxyz" ..
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ0" ..
    "123456789.,!?-+/():;%&`'*#=[]\"")
  
  background = love.graphics.newImage("sprites/bg.png")
  backgroundQuad = love.graphics.newQuad(1,1,720/2,1280/2,720/2,1280/2)
  
  mainScreen = love.graphics.newImage("sprites/ms.png")
  mainScreenQuad = love.graphics.newQuad(1,1,720/2,1280/2,720/2,1280/2)
  
  restartOver = love.graphics.newImage("sprites/restart.png")
  restartOverQuad = love.graphics.newQuad(1,1,720/2,1280/2,720/2,1280/2)
  
  flappy.img = love.graphics.newImage("sprites/flappy.png")
  flappy.y = 200
  flappy.x = 50
  flappy.y_velocity = 0
  flappy.jump_height = -300
  flappy.gravity = -800
  
  flappy.spaceIsDown = false

  ground = love.graphics.newImage("sprites/ground.png")
  groundQuad = love.graphics.newQuad(1,1,720/2,1280/10,720/2,1280/10) 
  groundPosY = 1280/2 - 1280/10
  
  gameOver = love.graphics.newImage("sprites/gOver.png")
  
  pipeN_1 = love.graphics.newImage("sprites/pipe.png")
  pipeN_2 = love.graphics.newImage("sprites/pipe.png")
  pipeN_3 = love.graphics.newImage("sprites/pipe.png")
  
  pipeF_1 = love.graphics.newImage("sprites/pipeFlip.png")
  pipeF_2 = love.graphics.newImage("sprites/pipeFlip.png")
  pipeF_3 = love.graphics.newImage("sprites/pipeFlip.png")
  
  pipesX_1 = 300
  pipesX_2 = 500
  pipesX_3 = 700
  
  pipeQuad = love.graphics.newQuad(1, 1, 60, 450, 60, 450) 
  pipePosY = groundPosY - 150
  
  pipesY_1 =  math.random(-450, -150)
  pipesY_2 = math.random(-450, -150)
  pipesY_3 = math.random(-450, -150)
  
  soundFlap = love.audio.newSource("sounds/Flap.wav", "static")
  soundPoint = love.audio.newSource("sounds/Point.wav", "static")
  soundHit = love.audio.newSource("sounds/Hit.wav", "static")
  
  

end

function love.update(dt)
  if current_state == "main_Menu" then
    main_screen()
  
  elseif current_state == "game" then
    game(dt)
    
  elseif current_state == "death" then
    death_screen()
  end
end

function game_screen()
   
  love.graphics.draw(pipeN_1, pipeQuad, pipesX_1, pipesY_1 +600)
  love.graphics.draw(pipeN_2, pipeQuad, pipesX_2, pipesY_2 +600)
  love.graphics.draw(pipeN_3, pipeQuad, pipesX_3, pipesY_3 +600)    
    
  love.graphics.draw(pipeF_1, pipeQuad, pipesX_1, pipesY_1)
  love.graphics.draw(pipeF_2, pipeQuad, pipesX_2, pipesY_2)
  love.graphics.draw(pipeF_3, pipeQuad, pipesX_3, pipesY_3)
        
  love.graphics.draw(flappy.img, flappy.x, flappy.y, angle)
  love.graphics.draw(ground, groundQuad,  0, groundPosY) 

  collisions()
end

function checkCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1 
end

function collisions()
  -- Ground Collision
  groundHit = checkCollision(0, groundPosY, 720/2, 1280/10, flappy.x, flappy.y, 45, 45)
  
  pipe1NHit = checkCollision(flappy.x, flappy.y, 30, 30, pipesX_1, pipesY_1 + 600, 60 , 450)
  pipe2NHit = checkCollision(flappy.x, flappy.y, 30, 30, pipesX_2, pipesY_2 + 600, 60 , 450)
  pipe3NHit = checkCollision(flappy.x, flappy.y, 30, 30, pipesX_3, pipesY_3 + 600, 60 , 450)
  
  pipe1FHit = checkCollision(flappy.x, flappy.y, 30, 30, pipesX_1, pipesY_1, 60 , 450)
  pipe2FHit = checkCollision(flappy.x, flappy.y, 30, 30, pipesX_2, pipesY_2, 60 , 450)
  pipe3FHit = checkCollision(flappy.x, flappy.y, 30, 30, pipesX_3, pipesY_3, 60 , 450)
  
  if(pipe1NHit or pipe1FHit ) then
    soundHit:play()
    current_state = "death"
  elseif(pipe2NHit or pipe2FHit ) then
    soundHit:play()
    current_state = "death"
  elseif(pipe3NHit or pipe3FHit ) then
    soundHit:play()
    current_state = "death"
  elseif(groundHit) then
    soundHit:play()
    current_state = "death"
  end 
end

function userInput(dt)
  
  if love.keyboard.isDown('space') and flappy.spaceIsDown == false then
    
    flappy.y_velocity = flappy.jump_height
    flappy.spaceIsDown = true
    soundFlap:play()
    angle = 0
    
  end
  
  if love.keyboard.isDown('space') == false then
    
    flappy.spaceIsDown = false 
    angle = angle + dt * math.pi / 4 
    
  end
  
end
function repeatingPipes()
  
  if currentScore < 25 then
    pipesX_1 = pipesX_1 - 1.5
    pipesX_2 = pipesX_2 - 1.5
    pipesX_3 = pipesX_3 - 1.5
  elseif currentScore < 50 then
    pipesX_1 = pipesX_1 - 2
    pipesX_2 = pipesX_2 - 2
    pipesX_3 = pipesX_3 - 2
  else
    pipesX_1 = pipesX_1 - 2.5
    pipesX_2 = pipesX_2 - 2.5
    pipesX_3 = pipesX_3 - 2.5
  end
        
  if pipesX_1 < -60 then
    soundPoint:play()
    currentScore = currentScore + 1
    pipesY_1 =  math.random(-450, -150)
    pipesX_1 = pipesX_3 + 200
  end
        
  if pipesX_2 < -60 then
    soundPoint:play()
    currentScore = currentScore + 1
    pipesY_2 = math.random(-450, -150)
    pipesX_2 = pipesX_1 + 200
  end
        
  if pipesX_3 < -60 then
    soundPoint:play()
    currentScore = currentScore + 1
    pipesY_3 = math.random(-450, -150)
    pipesX_3 = pipesX_2 + 200
  end
  
  
  
end

function reset()
  flappy.y = 200
  flappy.x = 50
    
  pipesX_1 = 300
  pipesX_2 = 500
  pipesX_3 = 700
end


function main_screen()
  function love.draw()
    love.graphics.draw(background, backgroundQuad, 0, 0)
    love.graphics.draw(mainScreen, mainScreenQuad, 0, 0)
  end
    if love.keyboard.isDown('space') then
    current_state = "game"  
  end
end

function game(dt)
  function love.draw()
    -- Main Background
    love.graphics.draw(background, backgroundQuad, 0, 0)

    repeatingPipes() 
    game_screen()
    
    love.graphics.setFont(font)
    love.graphics.scale(3,3)
    love.graphics.print(currentScore, 55, 0)    

  end
    userInput(dt)

    if flappy.y_velocity ~= 0 then                                      
      flappy.y = flappy.y + flappy.y_velocity * dt                
      flappy.y_velocity = flappy.y_velocity - flappy.gravity * dt
    end 
end

function death_screen()
  if currentScore > highScore then
    highScore = currentScore
  end
  currentScore = 0
  function love.draw()
      --love.graphics.scale(1,1)
      --love.graphics.print(highScore, 720/2, 1280/2)
      
      love.graphics.draw(background, backgroundQuad, 0, 0)
      love.graphics.draw(gameOver, 50, 50)
      love.graphics.draw(ground, groundQuad,  0, groundPosY) 
      love.graphics.draw(restartOver, restartOverQuad, 0, 0)
      love.graphics.print("High Score: "..highScore.." ", 720/10, 1280/8, 0 , 2, 2)


      --love.graphics.print(highScore, 30, 50)
  end
  
  if love.keyboard.isDown('space') then
    current_state = "game"  
    reset()
  end 
end

function love.touchpressed( id, x, y, dx, dy, pressure )
  if current_state == "main_Menu" then
    current_state = "game"  
  elseif current_state == "game" then
    flappy.y_velocity = flappy.jump_height
    flappy.spaceIsDown = true
    soundFlap:play()
    angle = 0
  elseif current_state == "death" then
    current_state = "game"  
    reset()
  end
end

function love.touchreleased( id, x, y, dx, dy, pressure )
    if current_state == "game" then
      angle = angle + dt * math.pi / 4 
    end
end







