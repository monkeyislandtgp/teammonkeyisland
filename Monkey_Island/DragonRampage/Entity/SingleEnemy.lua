SingleEnemy ={}
SingleEnemy.__index = SingleEnemy

function SingleEnemy.Create(posX, posY, speed)
  local self ={}
  setmetatable(self,SingleEnemy)
  self.Objects = {} 
  self.currentFrame = 1
  self.elapsed = 0

  self.px = posX
  self.py = posY
  self.ps = speed
  
  return self
end

function SingleEnemy:Load(health) 
  score = 0
  EnemyDeaths = 0
  self.PlayerHealth = health
  --Enemy
  -- Image and array for enemy firing pattern - Brandon
  self.enemyFireImg = love.graphics.newImage("Graphics/Sprites/Enemies/GriffinSlash.png")
  self.enemyFiress = {}
  
  self.GriffinSprite = love.graphics.newImage("Graphics/Sprites/Enemies/Griffin_SSheet.png")
  self.GriffinHeight = 113
  self.GriffinWidth = 173.75
  
  self.GriffinFrames = {
    love.graphics.newQuad(0, 0, self.GriffinWidth, self.GriffinHeight, self.GriffinSprite:getWidth(), self.GriffinSprite:getHeight()),
    love.graphics.newQuad(self.GriffinWidth, 0, self.GriffinWidth, self.GriffinHeight, self.GriffinSprite:getWidth(), self.GriffinSprite:getHeight()),
    love.graphics.newQuad((self.GriffinWidth*2), 0, self.GriffinWidth, self.GriffinHeight, self.GriffinSprite:getWidth(), self.GriffinSprite:getHeight()),
    love.graphics.newQuad((self.GriffinWidth*3), 0, self.GriffinWidth, self.GriffinHeight, self.GriffinSprite:getWidth(), self.GriffinSprite:getHeight()),
  }
  
  math.randomseed(os.time()) -- random seed for random generators - Brandon
  
  width = love.graphics.getWidth()
  height = love.graphics.getHeight()
  
  self.e = {}
  self.e.width = self.GriffinWidth 
  self.e.height = self.GriffinHeight
  self.e.x = self.px
  self.e.y = self.py
  self.e.speed = self.ps
  self.e.shootTimer = 0
  self.e.shootTimerMax = math.random(5,10)
  self.e.active = true
  
end

function SingleEnemy:Unload()

end

function SingleEnemy:Update(dt)
  --Sets the Animation Speed
  self.elapsed = self.elapsed + 0.1
  if self.elapsed > 1 then
      self.elapsed = self.elapsed - 1
      if self.currentFrame == 4 then
        self.currentFrame = 1
      else
        self.currentFrame = self.currentFrame + 1
      end
  end
  
  --Enemy
  if self.e.active == true then
    for i,b in ipairs(bullets) do
      if checkCollision(b.x * scaleX, b.y * scaleY, 50 * scaleX, 50 * scaleY,  self.e.x * scaleX, self.e.y * scaleY, 70 * scaleX , 70 * scaleY) then
        self.e.active = false
        ScoreCount = ScoreCount + 100
      end
    end
    
    if self.e.x < -100 then
      self.e.active = false 
    end
    
    self.e.x = self.e.x - self.e.speed
    
  end 

  if self.e.active == false then
    self.e.x = math.random(1400, 1800)
    self.e.y = math.random(100, 550)
    self.e.active = true
  end
    
    
    -- creates a delay between shots for enemy - Brandon
  if self.e.shootTimer < self.e.shootTimerMax then 
    self.e.shootTimer = self.e.shootTimer + math.random(2,5) * dt 
  else
    table.insert(self.enemyFiress, {x = self.e.x, y = self.e.y, s = 4})
    self.e.shootTimer = 0
  end     
  
  --Enemy Firing 
  for j, enemyFire in pairs(self.enemyFiress) do
    enemyFire.x = enemyFire.x - enemyFire.s + dt
  end
    
  
  for i, enemyFire in pairs (self.enemyFiress) do
    --Player death detection
    self.enemyFirehit  = checkCollision(enemyFire.x * scaleX, enemyFire.y * scaleY, 30 * scaleX , 30 * scaleY, PlayerPosX * scaleX, PlayerPosY * scaleY, 60 * scaleX, 60 * scaleY)
    if (self.enemyFirehit)then
      CurrentHealth = CurrentHealth - 1
      enemyFire.x = enemyFire.x -30000
    end
    
  end
  
end

function SingleEnemy:Respawn(respawn)
  self.e.active = respawn
end

function SingleEnemy:EnemyDetails()
  return self.e.x, self.e.y, self.e.width, self.e.height
end

function SingleEnemy:Draw()
  --Enemy
  love.graphics.draw(self.GriffinSprite, self.GriffinFrames[self.currentFrame], (self.e.x * scaleX), (self.e.y * scaleY), 0 ,(1*scaleX), (1*scaleY))
  for i, enemyFire in pairs (self.enemyFiress) do
    love.graphics.draw(self.enemyFireImg, (enemyFire.x * scaleX) , (enemyFire.y + 50 * scaleY), 0, (1 *scaleX), (1*scaleY), self.enemyFireImg:getWidth()/2, self.enemyFireImg:getHeight()/2)
  end
end
