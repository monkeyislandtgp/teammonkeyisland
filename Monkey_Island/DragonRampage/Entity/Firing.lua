Firing ={}
Firing.__index = Firing

function Firing.Create()
  local self ={}
  setmetatable(self,Firing)
  return self
end

function Firing:Load()
  firerate = 2
  cooldownTimer = 0
  bullets = {}
  bulletSpeed = 15
  bullet_lifespan = 100
end

function Firing:Unload()

end

function Firing:Update(dt)
  cooldownTimer = cooldownTimer - dt 

  while Fire == true and cooldownTimer <= 0 do 
    cooldownTimer = cooldownTimer + firerate
    Shoot()
  end

	for i,b in ipairs(bullets) do
		b.x = b.x + bulletSpeed
    b.lifespan = b.lifespan - 1
    if b.lifespan < 0 then 
      table.remove(bullets, i)
    end
	end
end

function Shoot()
  bullet = {}
  bullet.x = PlayerPosX + 70
  bullet.y = PlayerPosY + 50
  bullet.lifespan = 100
  bullet.graphic = love.graphics.newImage("Graphics/Sprites/Player/fireballSingle.png")
  table.insert(bullets, bullet)
end

function Firing:Draw()
  for i, b in ipairs(bullets) do
    love.graphics.draw(b.graphic, (b.x * scaleX), (b.y * scaleY) , 0 ,(1*scaleX), (1*scaleY)) 
  end
end
