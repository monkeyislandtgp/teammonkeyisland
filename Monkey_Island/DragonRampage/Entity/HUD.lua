HUD ={}
HUD.__index = HUD

function HUD.Create()
  local self ={}
  setmetatable(self,HUD)
  self.Objects = {} 
    
  self.PauseNormal = love.graphics.newImage("Graphics/Scenes/All/Pause_Button_Norm.png")
  self.PauseClicked = love.graphics.newImage("Graphics/Scenes/All/Pause_Button_Clicked.png")
  
  self.PauseButton = RolloverButton.Create(1240, 40 , self.PauseNormal, self.PauseClicked)

  return self
end

function HUD:Load()  
  HUDBar = love.graphics.newImage("Graphics/Scenes/All/top-bar.png")
  
  HUDPositionX = (1280 / 2) - HUDBar:getWidth( ) / 2
  HUDPositionY = 0
  
  
  HeartGraphic = love.graphics.newImage("Graphics/Scenes/All/heartSmall.png")
  HealthCount = 0
  ScoreCount = 0 
  CoolDownTimer = 0
end

function HUD:Unload()

end

function HUD:Update(dt)
  HealthCount = Player:GetHealth()
  self.PauseButton:Update(dt)
  ScoreCount = GameTime * 100
  if self.PauseButton:IsReleased() or self.PauseButton:IsClicked() then 
    pause = true
  end

end

function HUD:Draw()
  love.graphics.draw(HUDBar, (HUDPositionX * scaleX), (HUDPositionY * scaleY))
  
  love.graphics.print("Score: ", 810, 10, 0, 0.25 * scaleX, 0.25 * scaleY)
  love.graphics.print(math.floor(ScoreCount), 880, 5 , 0, 0.3 * scaleX, 0.3 * scaleY)
  
  love.graphics.print("Lives: ", 270, 10, 0, 0.25 * scaleX, 0.25 * scaleY)
  for i = 1, HealthCount do
    love.graphics.draw(HeartGraphic, 300 + (i * 35), 5)
    
  if pause == false then
    self.PauseButton:Draw()
  end
  end

end