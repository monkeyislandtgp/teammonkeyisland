Player ={}
Player.__index = Player

function Player.Create()
  local self ={}
  setmetatable(self,Player)
  self.Objects = {} 
  self.currentFrame = 1
  self.elapsed = 0
  playerFire = Firing.Create()
  return self
end

function Player:Load()  
  --All Values That will need to be read in from server. 
  DragonSwipeSpeed = false
  BDragonFireRate = false
  BDragonHealth = false
  
  CurrentSpeed = 3
  CurrentFireRate = 2
  CurrentHealth = 10
  
  --Changing Fire Rate Affects Distance it travels
  if (BDragonFireRate == true) then
    CurrentFireRate = self.CurrentFireRate / 2
  end
  
    ----------- Dragon --------------
  --PlayerFireBurst = love.graphics.newImage("Graphics/Sprites/Player/fireballSingle.png")

  PlayerPosY = 720/2 - 107
  PlayerPosX = 20
  PlayerHeight = 107
  PlayerWidth = 96
  
  Fire = false
  
  playerFire:Load()
  
  --Load Sprite Sheet
  PlayerSprite = love.graphics.newImage("Graphics/Sprites/Player/Dragon_SSheet.png")
  PlayerFrames = {
    love.graphics.newQuad(0, 0, PlayerWidth, PlayerHeight, PlayerSprite:getWidth(),  PlayerSprite:getHeight()),
    love.graphics.newQuad(96, 0, PlayerWidth, PlayerHeight, PlayerSprite:getWidth(), PlayerSprite:getHeight()),
    love.graphics.newQuad(192, 0, PlayerWidth, PlayerHeight, PlayerSprite:getWidth(), PlayerSprite:getHeight()),
    love.graphics.newQuad(288, 0, PlayerWidth, PlayerHeight, PlayerSprite:getWidth(), PlayerSprite:getHeight()),
  }
  
end

function Player:Unload()

end

function Player:Update(dt)

  -- Player Firing Mechanic
  playerFire:Update(dt)

  --Touch 
  temp = PlayerPosY

  if love.system.getOS() == "Android" then
    if TouchStartX < 1280 / 2 then 
      --Up
      if TouchEndY < TouchStartY then
        if PlayerPosY > 70 then
          PlayerPosY = temp - CurrentSpeed
        end
      end
      
      -- Down
      if TouchEndY > TouchStartY then
        if PlayerPosY < 550 then
          PlayerPosY = temp + CurrentSpeed
        end
      end
  end
    
    -- Tap on Right Side
    if TouchStartX > 1280 / 2 then 
      Fire = true
    else 
      Fire = false
    end
    
  elseif love.system.getOS() == "iOS" then
    --Up
    if TouchEndY < TouchStartY then
      if PlayerPosY > TouchEndY - PlayerPosY then
        PlayerPosY = temp - CurrentSpeed
      end
    end
    -- Down
    if TouchEndY > TouchStartY then
      if PlayerPosY < TouchEndY + PlayerPosY then
        PlayerPosY = temp + CurrentSpeed
      end
    end
    
  else
    -- Tap on Left Side
    if MouseEndX < 1280 / 2 then 
      --Down
      if MouseEndY < MouseStartY then
        if PlayerPosY > 70 then
          PlayerPosY = temp - CurrentSpeed
        end
      end
      -- Up
      if MouseEndY > MouseStartY then
        if PlayerPosY < 550 then 
          PlayerPosY = temp + CurrentSpeed
        end
      end
    end
    
    -- Tap on Right Side
    if MouseEndX > 1280 / 2 then 
      Fire = true
    else 
      Fire = false
    end
    
  end
  
  --Sets Player Pos if out of bounds 
  if PlayerPosY > 550  then
    PlayerPosY = 550
  end
  
  if PlayerPosY < 70 then
    PlayerPosY = 70
  end

  
  --Sets the Animation Speed
  self.elapsed = self.elapsed + 0.1
  if self.elapsed > 1 then
      self.elapsed = self.elapsed - 1
      if self.currentFrame == 4 then
        self.currentFrame = 1
      else
        self.currentFrame = self.currentFrame + 1
      end
  end
end

function Player:Draw()
  love.graphics.setColor(255, 255, 255, 255)
  love.graphics.draw(PlayerSprite, PlayerFrames[self.currentFrame], (PlayerPosX * scaleX), (PlayerPosY * scaleY), 0 ,(1*scaleX), (1*scaleY))
  
  playerFire:Draw()
end

function Player:GetHealth()
  return CurrentHealth
end

function Player:SetHealth(health)
  CurrentHealth = health
end

function Player:GetDragonPosX()
  return PlayerPosX
end

function Player:GetDragonPosY()
  return PlayerPosY
end

function Player:SetDragonPos(x, y)
  PlayerPosY = y
  PlayerPosX = x
end

function Player:GetDragonFireX()
  return PlayerFirePosX
end
function Player:GetDragonFireY()
  return PlayerFirePosY
end

function Player:SetDragonFire(x, y)
  PlayerFirePosX = x
  PlayerFirePosY = y
end


