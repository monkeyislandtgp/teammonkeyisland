Background ={}
Background.__index = Background

function Background.Create()
  local self ={}
  setmetatable(self,Background)
  self.Objects = {} 
  return self
end

function Background:Load()  
  fade = true
  alpha1 = 255
  alpha2 = 0
  Day = love.graphics.newImage("Graphics/Scenes/Game/Day.png")
  Night = love.graphics.newImage("Graphics/Scenes/Game/Night.png")
  

  Portal = love.graphics.newImage("Graphics/Scenes/All/Portal.png")

  frontBackgroundMtn1 = love.graphics.newImage("Graphics/Scenes/Game/Mountains.png")
  frontBackgroundMtn2 = love.graphics.newImage("Graphics/Scenes/Game/Mountains.png")
  
  dark = love.graphics.newImage("Graphics/Scenes/Game/Darkness.png")
  
  frontBackground1 = love.graphics.newImage("Graphics/Scenes/Game/Trees.png")
  frontBackground2 = love.graphics.newImage("Graphics/Scenes/Game/Trees.png")
  

  frontBackgroundMtn1Pos = 0
  frontBackgroundMtn2Pos = 1270
  frontBackground1Pos = 0
  frontBackground2Pos = 1270 * 2
  


end

function Background:Unload()

end

function Background:Update(dt)
  
  frontBackgroundMtn1Pos = frontBackgroundMtn1Pos - 0.6
  frontBackgroundMtn2Pos = frontBackgroundMtn2Pos - 0.6
  frontBackground1Pos = frontBackground1Pos - 1
  frontBackground2Pos = frontBackground2Pos -1 
  
  if fade == true then
    alpha1 = alpha1 - 0.1
    alpha2 = alpha2 + 0.1
    if alpha1 < 0  then
      alpha1 = 0
      alpha2 = 255
      if alpha1 == 0 then
        fade = false
      end
    end
  end
  
  if fade == false then
    alpha1 = alpha1 + 0.1
    alpha2 = alpha2 - 0.1
    if alpha1 > 255 then 
      alpha1 = 255
      alpha2 = 0
      if alpha1 == 255 then
        fade = true
      end
    end
  end
  
  --mountains
  if frontBackgroundMtn1Pos < -1270 then
    frontBackgroundMtn1Pos = 1270
  end
  if frontBackgroundMtn2Pos < -1270 then
    frontBackgroundMtn2Pos = 1270
  end
  --trees
  if frontBackground1Pos < -3786 then
    frontBackground1Pos = 3786
  end
  if frontBackground2Pos < -3786 then
    frontBackground2Pos = 3786
  end
end

function Background:Draw()
  ----------- Background --------------
  love.graphics.setColor(255, 255, 255, alpha1)
  love.graphics.draw(Day, (0 * scaleX), (0 * scaleY) , 0 ,(1*scaleX), (1*scaleY)) 
  
  love.graphics.setColor(255, 255, 255, alpha2)
  love.graphics.draw(Night, (0 * scaleX), (0 * scaleY) , 0 ,(1*scaleX), (1*scaleY)) 
  
  love.graphics.setColor(255, 255, 255, 255)
  love.graphics.draw(frontBackgroundMtn1, (frontBackgroundMtn1Pos * scaleX), (0 * scaleY) , 0 ,(1*scaleX), (1*scaleY)) 
  love.graphics.draw(frontBackgroundMtn2, (frontBackgroundMtn2Pos * scaleX), (0 * scaleY) , 0 ,(1*scaleX), (1*scaleY)) 
  love.graphics.draw(frontBackground1, (frontBackground1Pos * scaleX), (360 * scaleY) , 0 ,(1*scaleX), (1*scaleY)) 
  love.graphics.draw(frontBackground2, (frontBackground2Pos * scaleX), (360 * scaleY) , 0 ,(1*scaleX), (1*scaleY)) 

  
  love.graphics.setColor(255, 255, 255, alpha2)
  love.graphics.draw(dark, (0 * scaleX), (0 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  
  
  love.graphics.setColor(255, 255, 255, 255)
  love.graphics.draw(Portal, (0 * scaleX), (0 * scaleY) , 0 ,(1*scaleX), (1*scaleY)) 
  
end