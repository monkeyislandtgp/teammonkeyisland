Leaderboard ={}
Leaderboard.__index = Leaderboard

function Leaderboard.Create()
  local self ={}
  setmetatable(self,Leaderboard)
  self.Objects = {} 

  
  -- Loads Buttons
  self.BackNormal = love.graphics.newImage("Graphics/Scenes/All/BackNormal.png")
  self.BackClicked = love.graphics.newImage("Graphics/Scenes/All/BackPressed.png")
  self.BackButton = RolloverButton.Create(275 ,92 ,self.BackNormal,self.BackClicked)

  return self
end


function Leaderboard:Load()
  --Sets Index For Buttons
  self.BackButton:SetLevelIndex(1)

  --Createing Backgound
  self.Objects.Background_Texture = love.graphics.newImage("Graphics/Backgrounds/Background.png")
  self.Objects.Foreground_Border = love.graphics.newImage("Graphics/Backgrounds/Border.png")
  self.Objects.Foreground_Corners= love.graphics.newImage("Graphics/Backgrounds/BorderCorners.png")
  self.Objects.Title= love.graphics.newImage("Graphics/Titles/Leaderboard.png")
  self.Objects.FakeList= love.graphics.newImage("Graphics/Titles/HighscoreList.png")
end

function Leaderboard:Unload()

end

function Leaderboard:Update(dt)
  self.BackButton:Update(dt)
  
  if self.BackButton:IsReleased() or self.BackButton:IsClicked() then 
    Game_States:ChangeState(1)
  end
 
end

function Leaderboard:Draw()
  love.graphics.draw(self.Objects.Background_Texture, (0 * scaleX), (0 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  love.graphics.draw(self.Objects.Foreground_Border, (0 * scaleX), (0 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  love.graphics.draw(self.Objects.Foreground_Corners, (0 * scaleX), (0 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  love.graphics.draw(self.Objects.Title, ((638 - (self.Objects.Title:getWidth() / 2 )) * scaleX), ((92 - (self.Objects.Title:getHeight() / 2 )) * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  love.graphics.draw(self.Objects.FakeList, ((629 - (self.Objects.FakeList:getWidth() / 2 )) * scaleX), ((399 - (self.Objects.FakeList:getHeight() / 2 )) * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  
  self.BackButton:Draw()
end

