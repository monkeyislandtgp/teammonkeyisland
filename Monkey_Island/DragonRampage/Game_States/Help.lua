Help ={}
Help.__index = Help

function Help.Create()
  local self ={}
  setmetatable(self,Help)
  self.Objects = {}
  
  
  -- Loads Buttons
  self.BackNormal = love.graphics.newImage("Graphics/Scenes/All/BackNormal.png")
  self.BackClicked = love.graphics.newImage("Graphics/Scenes/All/BackPressed.png")
  self.BackButton = RolloverButton.Create(275 ,92 ,self.BackNormal,self.BackClicked)
  
  return self
end


function Help:Load()
  -- Help page background --
  self.Background_Texture = love.graphics.newImage("Graphics/Backgrounds/Background.png")
  self.Foreground_Border = love.graphics.newImage("Graphics/Backgrounds/Border.png")
  self.Foreground_Corners= love.graphics.newImage("Graphics/Backgrounds/BorderCorners.png")
  
  -- load images on screen ----
  self.Pointer = love.graphics.newImage("Graphics/Help/pointer.png")
  self.draground = love.graphics.newImage("Graphics/Help/draground.png")
  self.MoveHelp = love.graphics.newImage("Graphics/Help/Help.png")
  self.Fireball = love.graphics.newImage("/Graphics/Sprites/Player/fireballSingle.png")
  
  --- variables -----
  self.PointerY = 250
  self.FireballX = 750
  isMovingUp = false
  isMovingDown = true
   
end

function Help:Unload()
  
end

function Help:Update(dt)
  self.BackButton:Update(dt)
  
  if self.BackButton:IsReleased() or self.BackButton:IsClicked() then 
    Game_States:ChangeState(1)
  end
  
  --pointer finger moving up and down ---
  if isMovingDown == false then
    isMovingUp = true
  end
  
  if isMovingDown == true then
    self.PointerY = self.PointerY + 0.2   
    
    if self.PointerY >= 440 then
      isMovingDown = false
    end
  end
  
  if isMovingUp == true then
    self.PointerY = self.PointerY - 0.2
    
    if self.PointerY <= 151 then
      isMovingUp = false
      isMovingDown = true
    end
  end
  
  --- fireball moving right then resetting -----
  
  self.FireballX = self.FireballX + 0.5
  
  if self.FireballX >= 1100 then
    self.FireballX = 750
  end

end

function Help:Draw()
  love.graphics.draw(self.Background_Texture, (0 * scaleX), (0 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  love.graphics.draw(self.Foreground_Border, (0 * scaleX), (0 * scaleY) , 0 ,(1*scaleX), (1*scaleY)) 
  love.graphics.draw(self.Foreground_Corners, (0 * scaleX), (0 * scaleY) , 0 ,(1*scaleX), (1*scaleY)) 
  love.graphics.draw(self.MoveHelp, 50 * scaleX, 133 * scaleY, 0 ,(1*scaleX), (1*scaleY)) 
  love.graphics.draw(self.draground, 70 * scaleX, 150 * scaleY)
  love.graphics.draw(self.draground, 650* scaleX, 150 * scaleY)
  love.graphics.draw(self.Pointer, 100* scaleX, self.PointerY * scaleY)
  love.graphics.draw(self.Fireball, self.FireballX * scaleX, 290 * scaleY)


  
  self.BackButton:Draw()
end