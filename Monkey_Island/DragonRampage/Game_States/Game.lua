Game ={}
Game.__index = Game
math.randomseed(os.time())

function Game.Create()
  local self ={}
  setmetatable(self,Game)
  self.Objects = {} 
  Player = Player.Create()
  Enemies = {}
  self.Background = Background.Create()
  self.PauseOverlay = PauseOverlay.Create()
  self.HUD = HUD.Create()
 
  for i = 1, 4 do
    e = SingleEnemy.Create(math.random(1300, 1700), math.random(100, 550), math.random(0.5,1.5))
    table.insert(Enemies, e)
  end
  
  return self
end

function Game:Load()
  TouchEndX = 0
  TouchEndY = 0
  TouchStartX = 0
  TouchStartY = 0
  
  GameTime = 0
  
  Player:Load()
  
  for i, newEnemy in ipairs(Enemies) do
    newEnemy:Load(Player:GetHealth()) 
  end
  
  self.Background:Load()
  self.PauseOverlay:Load()
  self.HUD:Load()
  
end

function Game:Unload()
  
end

function Game:Update(dt)
  if pause == false then 
    GameTime = GameTime + dt
    Player:Update(dt)
    self.Background:Update(dt)
    self.HUD:Update()
    
    --Spawns All Enemies
    for i, newEnemy in ipairs(Enemies) do
      newEnemy:Update(dt)
    end
  end

  if pause == true then 
    self.PauseOverlay:Update(dt)
  end

  if (CurrentHealth == 0) then
    Game_States:ChangeState(4)
  end



end

function Game:Draw()
----------- Dragon --------------
  self.Background:Draw()
  Player:Draw()
  
  for i, newEnemy in ipairs(Enemies) do
    newEnemy:Draw()
  end

  if pause == true then
    self.PauseOverlay:Draw()
  end
  
  self.HUD:Draw() 
end