GameOver ={}
GameOver.__index = GameOver

function GameOver.Create()
  local self ={}
  setmetatable(self,GameOver)
  self.Objects = {} 
  
  self.HomeNormal = love.graphics.newImage("Graphics/Scenes/All/Home_Normal.png")
  self.HomePressed = love.graphics.newImage("Graphics/Scenes/All/Home_Pressed.png")
  self.HomeButton = RolloverButton.Create(639.5 ,619 ,self.HomeNormal,self.HomePressed)
  
  return self
end


function GameOver:Load()
  --Createing Backgound
  self.Objects.Background_Texture = love.graphics.newImage("Graphics/Backgrounds/Background.png")
  self.Objects.Foreground_Border = love.graphics.newImage("Graphics/Backgrounds/Border.png")
  self.Objects.Foreground_Corners= love.graphics.newImage("Graphics/Backgrounds/BorderCorners.png")
  self.Objects.UC = love.graphics.newImage("Graphics/Scenes/All/UC.png")
end

function GameOver:Unload()

end

function GameOver:Update(dt)
  self.HomeButton:Update(dt)
  
  if self.HomeButton:IsReleased() or self.HomeButton:IsClicked() then 
    Game_States:ChangeState(1)
  end
end

function GameOver:Draw()
  love.graphics.draw(self.Objects.Background_Texture, (0 * scaleX), (0 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  love.graphics.draw(self.Objects.Foreground_Border, (0 * scaleX), (0 * scaleY) , 0 ,(1*scaleX), (1*scaleY)) 
  love.graphics.draw(self.Objects.Foreground_Corners, (0 * scaleX), (0 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  love.graphics.draw(self.Objects.UC, (0 * scaleX), (0 * scaleY) , 0 ,(1*scaleX), (1*scaleY)) 
  love.graphics.print("Score: ", (1280/2 - 150) * scaleX, (720/2 + 100) * scaleY, 0, 0.5 * scaleX, 0.5 *scaleY)
  love.graphics.print(math.floor(ScoreCount), (1280/2) * scaleX, (720/2 + 100) * scaleY , 0, 0.5 * scaleX, 0.5 *scaleY)
  
  self.HomeButton:Draw()
end