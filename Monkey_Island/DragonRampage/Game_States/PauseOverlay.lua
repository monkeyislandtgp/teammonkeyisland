PauseOverlay ={}
PauseOverlay.__index = PauseOverlay

function PauseOverlay.Create()
  local self ={}
  setmetatable(self,PauseOverlay)
  self.Objects = {} 
  
  self.ResumeNormal = love.graphics.newImage("Graphics/Scenes/Pause/Resume_Norm.png")
  self.ResumeClicked = love.graphics.newImage("Graphics/Scenes/Pause/Resume_Pressed.png")
  self.ResumeButton = RolloverButton.Create( 638.5, 346, self.ResumeNormal, self.ResumeClicked)
  
  self.ExitNormal = love.graphics.newImage("Graphics/Scenes/Pause/Exit_Norm.png")
  self.ExitClicked = love.graphics.newImage("Graphics/Scenes/Pause/Exit_Pressed.png")
  self.ExitButton = RolloverButton.Create( 639.5, 439, self.ExitNormal, self.ExitClicked)
  
  return self
end

function PauseOverlay:Load()
  TouchX = 0 
  TouchY = 0
  TouchStartY = 0 
  TouchEndY = 0
  
  self.Objects.PauseMain = love.graphics.newImage("Graphics/Scenes/Pause/Pause_Menu.png")
  self.Objects.DragonAndFire = love.graphics.newImage("Graphics/Scenes/Pause/DragonDetail.png")
end

function PauseOverlay:Unload()

end

function PauseOverlay:Update(dt)
  self.ExitButton:Update(dt)
  self.ResumeButton:Update(dt)
  
  if self.ResumeButton:IsReleased() or self.ResumeButton:IsClicked() then 
    pause = false
  end
  
  if self.ExitButton:IsReleased() or self.ExitButton:IsClicked() then 
    pause = false
    Game_States:ChangeState(1)
  end
  
end

function PauseOverlay:Draw()
  love.graphics.draw(self.Objects.PauseMain, (0 * scaleX), (0 * scaleY) , 0 ,(1*scaleX), (1*scaleY)) 
  love.graphics.draw(self.Objects.DragonAndFire, (0 * scaleX), (0 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  
  self.ResumeButton:Draw()
  self.ExitButton:Draw()
end
