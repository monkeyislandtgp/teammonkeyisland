BoosterMenu ={}
BoosterMenu.__index = BoosterMenu

function BoosterMenu.Create()
  local self ={}
  setmetatable(self,BoosterMenu)
  self.Objects = {} 
  
  self.BackNormal = love.graphics.newImage("Graphics/Scenes/All/BackNormal.png")
  self.BackClicked = love.graphics.newImage("Graphics/Scenes/All/BackPressed.png")
  self.BackButton = RolloverButton.Create(275 ,92 ,self.BackNormal,self.BackClicked)
  
  self.StartNormal = love.graphics.newImage("Graphics/Scenes/Shop/Start_But_Norm.png")
  self.StartClicked = love.graphics.newImage("Graphics/Scenes/Shop/Start_But_Pressed.png")
  self.StartButton = RolloverButton.Create(639.5 ,619 ,self.StartNormal,self.StartClicked)
  
  self.IncNormal = love.graphics.newImage("Graphics/Scenes/Shop/Inc_But_Norm.png")
  self.IncClicked = love.graphics.newImage("Graphics/Scenes/Shop/Inc_But_Pressed.png")
  
  self.DecNormal = love.graphics.newImage("Graphics/Scenes/Shop/Dec_But_Norm.png")
  self.DecClicked = love.graphics.newImage("Graphics/Scenes/Shop/Dec_But_Pressed.png")
  
  self.Inc1 = RolloverButton.Create(1014 ,181 ,self.IncNormal,self.IncClicked)
  self.Inc2 = RolloverButton.Create(1014 ,352 ,self.IncNormal,self.IncClicked)
  self.Inc3 = RolloverButton.Create(1014 ,520 ,self.IncNormal,self.IncClicked)
  
  self.Dec1 = RolloverButton.Create(806 ,181 ,self.DecNormal,self.DecClicked)
  self.Dec2 = RolloverButton.Create(806 ,352 ,self.DecNormal,self.DecClicked)
  self.Dec3 = RolloverButton.Create(806 ,520 ,self.DecNormal,self.DecClicked)
            
  return self
end


function BoosterMenu:Load()
  --Createing Backgound
  self.Objects.Background_Texture = love.graphics.newImage("Graphics/Backgrounds/Background.png")
  self.Objects.Foreground_Border = love.graphics.newImage("Graphics/Backgrounds/Border.png")
  self.Objects.Foreground_Corners= love.graphics.newImage("Graphics/Backgrounds/BorderCorners.png")
  
  self.Objects.Title = love.graphics.newImage("Graphics/Scenes/Shop/Shop.png")
  self.Objects.Divide = love.graphics.newImage("Graphics/Scenes/Shop/Divide.png")
  self.Objects.Coin = love.graphics.newImage("Graphics/Scenes/All/Coin.png")
  
  self.Objects.IncreasePrice = love.graphics.newImage("Graphics/Scenes/Shop/100.png")
  self.Objects.DecreasePrice= love.graphics.newImage("Graphics/Scenes/Shop/50.png")
  
  self.Objects.FireInc = love.graphics.newImage("Graphics/Scenes/Shop/Icon_Bottom_Upg.png")
  self.Objects.HealthInc = love.graphics.newImage("Graphics/Scenes/Shop/Icon_Top_Upg.png")
  self.Objects.SwipeInc= love.graphics.newImage("Graphics/Scenes/Shop/Icon_Middle_Upg.png")
  
  self.Objects.CoinBst = love.graphics.newImage("Graphics/Scenes/Shop/Coin_But_Bst.png")
  self.Objects.HealthBst = love.graphics.newImage("Graphics/Scenes/Shop/Health_But_Bst.png")
  self.Objects.SwipeBst= love.graphics.newImage("Graphics/Scenes/Shop/Swipe_But_Bst.png")
  
  self.Objects.CoinBstSelec = love.graphics.newImage("Graphics/Scenes/Shop/Coin_But_Bst_Pressed.png")
  self.Objects.HealthBstSelec = love.graphics.newImage("Graphics/Scenes/Shop/Health_But_Bst_Pressed.png")
  self.Objects.SwipeBstSelec= love.graphics.newImage("Graphics/Scenes/Shop/Swipe_But_Bst_Pressed.png")
  
  self.Objects.SwipeBstPrice = love.graphics.newImage("Graphics/Scenes/Shop/Swipe_Det.png")
  self.Objects.HealthBstPrice = love.graphics.newImage("Graphics/Scenes/Shop/Health_Det.png")
  self.Objects.CoinBstPrice= love.graphics.newImage("Graphics/Scenes/Shop/c.png")
  
end

function BoosterMenu:Unload()

end

function BoosterMenu:Update(dt)
  
  self.BackButton:Update(dt)
  self.StartButton:Update(dt)
  
  if self.BackButton:IsReleased() or self.BackButton:IsClicked() then 
    Game_States:ChangeState(1)
  end
  
  if self.StartButton:IsReleased() or self.StartButton:IsClicked() then 
    Game_States:ChangeState(3)
  end
  
  self.Inc1:Update(dt)
  self.Inc2:Update(dt)
  self.Inc3:Update(dt)
  
  if self.Inc1:IsReleased() or self.Inc1:IsClicked() then 
    IncrementRate = IncrementRate + 1
  end
  
  if self.Inc2:IsReleased() or self.Inc2:IsClicked() then 
    IncrementHealth = IncrementHealth + 1
  end
  
  if self.Inc3:IsReleased() or self.Inc3:IsClicked() then 
    IncrementSwipe = IncrementSwipe + 1
  end
  
  self.Dec1:Update(dt)
  self.Dec2:Update(dt)
  self.Dec3:Update(dt)
  
    
  if self.Dec1:IsReleased() or self.Dec1:IsClicked() then 
    IncrementRate = IncrementRate - 1
  end
  
  if self.Dec2:IsReleased() or self.Dec2:IsClicked() then 
    IncrementHealth = IncrementHealth - 1
  end
  
  if self.Dec3:IsReleased() or self.Dec3:IsClicked() then 
    IncrementSwipe = IncrementSwipe - 1
  end
  
  if IncrementRate < 1 then 
    IncrementRate = 1
  end
  
  if IncrementHealth < 1 then 
    IncrementHealth = 1
  end
  
  if IncrementSwipe < 1 then 
    IncrementSwipe = 1
  end
  
    if IncrementRate > 9 then 
    IncrementRate = 9
  end
  
  if IncrementHealth > 9 then 
    IncrementHealth = 9
  end
  
  if IncrementSwipe > 9 then 
    IncrementSwipe = 9
  end
  
  
  
  
  
end

function BoosterMenu:Draw()
  love.graphics.draw(self.Objects.Background_Texture, (0 * scaleX), (0 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  love.graphics.draw(self.Objects.Foreground_Border, (0 * scaleX), (0 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  love.graphics.draw(self.Objects.Foreground_Corners, (0 * scaleX), (0 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  
  
  love.graphics.draw(self.Objects.Title, (527 * scaleX), (34 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  love.graphics.draw(self.Objects.Divide, (0 * scaleX), (0 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  love.graphics.draw(self.Objects.Coin, (0 * scaleX), (0 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  love.graphics.print(CurrentCoin, 180* scaleX, 535 * scaleY , 0, 0.8 * scaleX, 0.8 * scaleY)
  
  
  love.graphics.draw(self.Objects.IncreasePrice, (1058 * scaleX), (167 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  love.graphics.draw(self.Objects.DecreasePrice, (670 * scaleX), (167 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  
  love.graphics.draw(self.Objects.IncreasePrice, (1058 * scaleX), (338 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  love.graphics.draw(self.Objects.DecreasePrice, (670 * scaleX), (338 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  
  love.graphics.draw(self.Objects.IncreasePrice, (1058 * scaleX), (508 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  love.graphics.draw(self.Objects.DecreasePrice, (670 * scaleX), (508 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  
  love.graphics.draw(self.Objects.FireInc, (850 * scaleX), (85 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  love.graphics.print(IncrementRate, 900 * scaleX, 72 * scaleY, 0, 0.75 * scaleX, 0.75 * scaleY)
  love.graphics.draw(self.Objects.HealthInc, (850 * scaleX), (254* scaleY) , 0 ,(1*scaleX), (1*scaleY))
  love.graphics.print(IncrementHealth, 900 * scaleX, 240 * scaleY , 0, 0.75 * scaleX, 0.75 * scaleY)
  love.graphics.draw(self.Objects.SwipeInc, (850 * scaleX), (423 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  love.graphics.print(IncrementSwipe, 900 * scaleX, 408 * scaleY , 0, 0.75 * scaleX, 0.75 * scaleY)
  
  love.graphics.draw(self.Objects.CoinBst, (55 * scaleX), (215 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  love.graphics.draw(self.Objects.HealthBst, (260 * scaleX), (215 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  love.graphics.draw(self.Objects.SwipeBst, (463 * scaleX), (215 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  
  love.graphics.draw(self.Objects.SwipeBstPrice, (44 * scaleX), (348 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  love.graphics.draw(self.Objects.HealthBstPrice, (264 * scaleX), (348 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  love.graphics.draw(self.Objects.CoinBstPrice, (479* scaleX), (348 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  
  self.BackButton:Draw()
  self.StartButton:Draw()
  
  self.Inc1:Draw()
  self.Inc2:Draw()
  self.Inc3:Draw()
  
  self.Dec1:Draw()
  self.Dec2:Draw()
  self.Dec3:Draw()
end