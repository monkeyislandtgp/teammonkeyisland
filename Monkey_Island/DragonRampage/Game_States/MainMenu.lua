MainMenu ={}
MainMenu.__index = MainMenu

function MainMenu.Create()
  local self ={}
  setmetatable(self,MainMenu)
  self.Objects = {} 
  
  self.PlayNormal = love.graphics.newImage("Graphics/Scenes/Main/PlayNormal.png")
  self.PlayClicked = love.graphics.newImage("Graphics/Scenes/Main/PlayPressed.png")
  self.PlayButton = RolloverButton.Create(1280/2 ,350 ,self.PlayNormal,self.PlayClicked)
  
  self.LeadorboardNormal = love.graphics.newImage("Graphics/Scenes/Main/Leadorboard.png")
  self.LeadorboardClicked = love.graphics.newImage("Graphics/Scenes/Main/LeadorboardPressed.png")
  self.LeadorboardButton = RolloverButton.Create(1280/2,475,self.LeadorboardNormal,self.LeadorboardClicked)
  
  self.HelpNormal = love.graphics.newImage("Graphics/Scenes/Main/Help.png")
  self.HelpClicked = love.graphics.newImage("Graphics/Scenes/Main/HelpPressed.png")
  self.HelpButton = RolloverButton.Create(1280/2,600 ,self.HelpNormal,self.HelpClicked)
  
  return self
end


function MainMenu:Load()
  love.graphics.clear()
  
  TouchEndX = 0
  TouchEndY = 0
  TouchStartX = 0
  TouchStartY = 0

  --Createing Backgound
  self.Objects.Background_Texture = love.graphics.newImage("Graphics/Backgrounds/Background.png")
  self.Objects.Foreground_Border = love.graphics.newImage("Graphics/Backgrounds/Border.png")
  self.Objects.Foreground_Corners= love.graphics.newImage("Graphics/Backgrounds/BorderCorners.png")
  self.Objects.Title= love.graphics.newImage("Graphics/Titles/Logo.png")
 
end

function MainMenu:Unload()
  
end

function MainMenu:Update(dt)
  
  self.PlayButton:Update(dt)
  self.LeadorboardButton:Update(dt)
  self.HelpButton:Update(dt)
  
  if self.PlayButton:IsReleased() or self.PlayButton:IsClicked() then 
    Game_States:ChangeState(2)
  end
  
  if self.LeadorboardButton:IsReleased() or self.LeadorboardButton:IsClicked() then 
    Game_States:ChangeState(5)
  end
  
  if self.HelpButton:IsReleased() or self.HelpButton:IsClicked() then 
    Game_States:ChangeState(6)
  end
end

function MainMenu:Draw()
  love.graphics.draw(self.Objects.Background_Texture, (0 * scaleX), (0 * scaleY) , 0 ,(1*scaleX), (1*scaleY))
  love.graphics.draw(self.Objects.Foreground_Border, (0 * scaleX), (0 * scaleY) , 0 ,(1*scaleX), (1*scaleY)) 
  love.graphics.draw(self.Objects.Foreground_Corners, (0 * scaleX), (0 * scaleY) , 0 ,(1*scaleX), (1*scaleY)) 
  love.graphics.draw(self.Objects.Title, ((1280/2 - (706/2)) * scaleX), (50 * scaleY) , 0 ,(1*scaleX), (1*scaleY)) 
  
  self.PlayButton:Draw()
  self.LeadorboardButton:Draw()
  self.HelpButton:Draw()
end
