-- GameState
require("State_Managing/GameState")
require("State_Managing/State")

--States
require("Game_States/MainMenu")
require("Game_States/Game")
require("Game_States/GameOver")
require("Game_States/BoosterMenu")
require("Game_States/Leaderboard")
require("Game_States/Help")
require("Game_States/PauseOverlay")

--Interface
require("Interface/RolloverButton")
require("Interface/Math")

--Character & Enemies
require("Entity/Player")
require("Entity/Background")
require("Entity/SingleEnemy")
require("Entity/Firing")
require("Entity/HUD")

--Sound
require("Sound/SoundManager")

TouchEndX = 0
TouchEndY = 0
TouchStartX = 0
TouchStartY = 0

MouseStartX = 0
MouseStartY = 0
MouseEndX = 0
MouseEndY = 0

mousePress = false

function love.load()
  pause = false
  CurrentCoin = 9999
  IncrementSwipe = 1
  IncrementHealth= 1
  IncrementRate = 1
  
  
  --Font 
  font = love.graphics.newFont("Graphics/SitkaB.ttc", 80)
  love.graphics.setFont(font)
  
  MouseStartY = 0 
  MouseEndY = 0 
  
  --Mobile
  width = 1280
  height = 720
  if love.system.getOS() == "Android" then
    local x, y = love.graphics.getDimensions()
    scaleX = x / width
    scaleY = y / height
  elseif love.system.getOS() == "iOS" then
    local x, y = love.graphics.getDimensions()
    scaleX = x / width
    scaleY = y / height
  else
    scaleX = 1
    scaleY = 1
  end
  
  Game_States = GameState.Create(1)
  
  -- Main Menu (1)
  Game_States:AddState(State.Create("Main Menu",MainMenu.Create()))
  
  -- Upgrade Menu (2)
  Game_States:AddState(State.Create("Booster Menu",BoosterMenu.Create()))
  
  -- Game (3)
  Game_States:AddState(State.Create("Game",Game.Create()))
  
  -- Game Over (4)
  Game_States:AddState(State.Create("Game Over",GameOver.Create()))
  
  --Leaderboard (5)
  Game_States:AddState(State.Create("Leaderboard",Leaderboard.Create()))
  
  --Help (6)
  Game_States:AddState(State.Create("Help",Help.Create()))

  --Mobile 
  --love.window.setMode(width * scaleX, height *scaleY)
end

function love.update(dt)
  Game_States:Update(dt)
end

function love.draw()
  --love.graphics.scale(scaleX,scaleY)
  Game_States:Draw()
end

function love.touchpressed(id, x, y, pressure )
  if love.system.getOS() == "Android" then
    TouchStartX = x * love.graphics.getWidth()
    TouchStartY = y * love.graphics.getHeight()
    press = true
  elseif love.system.getOS() == "iOS" then
    TouchStartX = x
    TouchStartY = y 
    press = true
  else
    TouchStartX = x * scaleX
    TouchStartY = y * scaleY
    press = true
  end
end

function love.touchreleased( id, x, y, pressure )
  if love.system.getOS() == "Android" then
    TouchEndX = x * love.graphics.getWidth()
    TouchEndY = y * love.graphics.getHeight()
    press = false
  elseif love.system.getOS() == "iOS" then
    TouchEndX = x
    TouchEndY = y
    press = false
  else
    TouchEndX = x * scaleX
    TouchEndY = y * scaleY
    press = false
  end
end

function love.mousepressed( x, y, button, isTouch )
  MouseStartX = x
  MouseStartY = y
  press = true
end

function love.mousereleased( x, y, button, isTouch )
  MouseEndX = x
  MouseEndY = y
  MouseX = x
  MouseY = y
  press = false
end

function love.keypressed(key)
  if(key == "1") then
    Game_States:ChangeState(1)
  end
  
  if(key == "2") then
    Game_States:ChangeState(2)
  end
  
  if(key == "3") then
    Game_States:ChangeState(3)
  end
  
  if(key == "4") then
    Game_States:ChangeState(4)
  end
  
  if(key == "5") then
    Game_States:ChangeState(5)
  end
  
  if(key == "6") then
    Game_States:ChangeState(6)
  end
end
