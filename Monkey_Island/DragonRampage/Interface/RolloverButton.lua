
RolloverButton = {}
RolloverButton.__index = RolloverButton

function RolloverButton.Create(x, y, NormalGraphic, HoverGraphic)
  local self ={}
  setmetatable(self,RolloverButton)
  self.X = x
  self.Y = y
  self.Normal_Graphic = NormalGraphic
  self.Hover_Graphic = HoverGraphic
  self.Level = 0
  self.Inside = false
  self.Released = false
  self.Clicked = false
  self.Scale = 1
  self.cx = 0
  self.cy = 0
    
  self.bhp = false
  self.bcp = false
  


  --self.buttonHovered = love.audio.newSource("assets/Sound/ButtonClick.wav")
  --self.buttonHovered:setLooping(false)
  --self.buttonClicked = love.audio.newSource("assets/Sound/ButtonHover.wav")
  
  return self
end

function RolloverButton:Load()

end

function RolloverButton:Unload()
  for index,value in ipairs(self) do
    value:UnloadContent()
  end
end

function RolloverButton:Update(dt)

  if love.system.getOS() == "Android" then
    if Contains(TouchStartX, TouchStartY, self.X  * scaleX, self.Y * scaleY ,self.Normal_Graphic:getWidth(),self.Normal_Graphic:getHeight(),1) then
      self.Inside = true
      if Contains(TouchEndX, TouchEndY, self.X  * scaleX, self.Y * scaleY ,self.Normal_Graphic:getWidth(),self.Normal_Graphic:getHeight(),1) then
          self.Released = true
      end
    else
      self.Released = false
      self.Inside = false
    end
  elseif love.system.getOS() == "iOS" then
    if Contains(TouchStartX, TouchStartY, self.X  * scaleX, self.Y * scaleY ,self.Normal_Graphic:getWidth(),self.Normal_Graphic:getHeight(),1) then
      self.Inside = true
      if Contains(TouchEndX, TouchEndY, self.X  * scaleX, self.Y * scaleY ,self.Normal_Graphic:getWidth(),self.Normal_Graphic:getHeight(),1) then
          self.Released = true
      end
    else
      self.Released = false
      self.Inside = false
    end
  else
    self.cx, self.cy = love.mouse.getPosition()
      
    if Contains(self.cx, self.cy, self.X  * scaleX, self.Y * scaleY ,self.Normal_Graphic:getWidth(),self.Normal_Graphic:getHeight(),1) then
      self.Inside = true
      if love.mouse.isDown(1) then
        self.Clicked =  true
      end
    else
      self.Inside = false
      self.Clicked = false
    end
  end
  

  
end
  
function RolloverButton:SetLevelIndex(level)
  self.Level = level
end

function RolloverButton:IsClicked()
  return self.Clicked
end

function RolloverButton:IsReleased()
  return self.Released
end

function RolloverButton:SetScale(value)
  self.Scale = value
end

function RolloverButton:Draw()
  if(not self.Inside) then
    love.graphics.draw(self.Normal_Graphic,(self.X * scaleX), (self.Y * scaleY), 0, (self.Scale * scaleX), (self.Scale * scaleY),self.Normal_Graphic:getWidth()/2,self.Normal_Graphic:getHeight()/2)
  else
     love.graphics.draw(self.Hover_Graphic,(self.X * scaleX),(self.Y * scaleY),0,(self.Scale * scaleX),(self.Scale * scaleY),self.Hover_Graphic:getWidth()/2,self.Hover_Graphic:getHeight()/2)
  end
end


--Button Mouse Colission
function Contains(m_x,m_y,p_x , p_y , width, height,Scale)
  local OriginalX = p_x - (width/2)
  local OriginalY = p_y - (height/2)
  local MaxX = OriginalX + width
  local MaxY = OriginalY + height
  
  if(m_x > OriginalX  and m_y > OriginalY)then
    if(MaxX  > m_x and MaxY  > m_y) then
        return true
      end
  end
  return false
end
